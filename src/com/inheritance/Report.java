package com.inheritance;

public class Report extends Student{
	//Store ptage and pass and also the attributes of student class(super keyword)
	int ptage;
	String pass;
	
	public Report(String name,int adNo,int std,int ptage,String pass){
	    super(name,adNo,std);
	    this.ptage = ptage;
	    this.pass = pass;
	}
	public int getptage(){
	    return this.ptage;
	  	}
	 public String getpass(){
	    return this.pass;  
	 }    
}
