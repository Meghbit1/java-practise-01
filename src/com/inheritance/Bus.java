package com.inheritance;

public class Bus extends Student{
	
	//Store route and BG and also the attributes of student class(super keyword)
		int route;
		String bloodG;
		
		public Bus(String name,int adNo,int std,int route,String bloodG){
		    super(name,adNo,std);
		    this.route = route;
		    this.bloodG = bloodG;
		}
		
		public int getroute(){
		    return this.route;
		  	}
		 public String getbloodG(){
		    return this.bloodG;  
		 }    
	

}
