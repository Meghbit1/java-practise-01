package com.inheritance;

public class Student {
	//Store name, ANo, STD
		String name;
		int adNo;
		int std;
		
		public Student(String name,int adNo,int std){
		    this.name = name;
		    this.adNo = adNo;
		    this.std  = std;
		}
		
		
		public String getName(){
		    return this.name;
		}
		
		public int getadNo(){
		    return this.adNo;
		  	}
		 public int getstd(){
		    return this.std; 
		 }

}
