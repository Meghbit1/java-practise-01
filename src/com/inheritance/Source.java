package com.inheritance;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Source {
	
	public static void main(String[] args) throws IOException {
    	BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    	String[] strNums1, strNums2;
    	strNums1 = br.readLine().split("\\s");
    	int adnoValue1 = Integer.parseInt(strNums1[1]); 
        int stdValue1  = Integer.parseInt(strNums1[2]);
        int ptageValue1 = Integer.parseInt(strNums1[3]);
        int routeValue1 = Integer.parseInt(strNums1[5]);
        strNums2 = br.readLine().split("\\s");
        int adnoValue2 = Integer.parseInt(strNums2[1]); 
        int stdValue2  = Integer.parseInt(strNums2[2]);
        int ptageValue2 = Integer.parseInt(strNums2[3]);
        int routeValue2 = Integer.parseInt(strNums2[5]);
        
    	/*Store these details by making objects of both classes and then with the help of if-else, return the correct output.*/
        Report r1 = new Report(strNums1[0],adnoValue1,stdValue1,ptageValue1,strNums1[4]);  
        Bus b1 = new Bus(strNums1[0],adnoValue1,stdValue1,routeValue1,strNums1[6]);
        Report r2 = new Report(strNums2[0],adnoValue2,stdValue2,ptageValue2,strNums2[4]);
        Bus b2 = new Bus(strNums2[0],adnoValue2,stdValue2,routeValue2,strNums2[6]);
        int input = Integer.parseInt(br.readLine());
        if(input == adnoValue1) {
           System.out.print(r1.getName() +" "+r1.getptage()+" "+b1.getroute());    
         }
         else if(input == adnoValue2){
              System.out.print(r2.getName() +" "+r2.getptage()+" "+b2.getroute());  
         }
         else{
             System.out.print("No entry found");  
         }

	}
}