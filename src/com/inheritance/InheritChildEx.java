package com.inheritance;

public class InheritChildEx extends InheritanceEx {
	
	public void testChild() {
		
		System.out.println("I am in test child");
	}
	
     
	public void example() {
		super.example();
		System.out.println("I am in example child");
	}
	
	public static void main(String args[]) {
		
		InheritanceEx obj = new InheritanceEx();
		
		obj.example();
		obj.test();
		
		System.out.println("-------------------------------------");
		
		InheritChildEx obj1 = new InheritChildEx();
		
		obj1.example();
		obj1.test();
		obj1.testChild();
		
		System.out.println("-------------------------------------");
		
		InheritanceEx obj2 = new InheritChildEx();
		
		obj2.test();
		//obj2.testChild(); - cannot access testChild -- runtime polymor
		obj2.example();
		
		System.out.println("-------------------------------------");
	}
	
}