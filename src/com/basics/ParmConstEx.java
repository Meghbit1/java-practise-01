package com.basics;

public class ParmConstEx {
	
	int x;
	
	public ParmConstEx(int x) {
		
		this.x = x;
	}

	public void display() {
		
		System.out.println(x);
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		ParmConstEx pcs = new ParmConstEx(8);
		ParmConstEx pcs1 = new ParmConstEx(9);
		
		pcs.display();
		pcs1.display();

	}

}
