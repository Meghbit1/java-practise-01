package com.basics;

public class ConstEx {
	
	int x;
	
	public ConstEx() {
		
		x = 12;
	}

	public void display() {
		
		System.out.println("My name is Meghana");
	}
	
	public static void info() {
		
		System.out.println("I am new to Java!!!!");
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ConstEx cone = new ConstEx();
		ConstEx cone1 = new ConstEx();
		
		
		//int value;
		//value = cone.x;
		
		//System.out.println("Number is : " + value);
		
		System.out.println("Number is : " + cone.x + ", " + cone1.x);
		
		cone.display();
		
		info();
		
		System.out.println("End of program");
		
	}
}