package com.basics;

public class ObjectEx {
	
	  int num = 10;                 //Non-static or Instance variables
	  String name = "Meghana";

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ObjectEx obje = new ObjectEx();
		
	//	obje.num;  error?
	//	obje.name; error?
		
		System.out.println(obje.num);
		System.out.println(obje.name);
	
		
	}

}
